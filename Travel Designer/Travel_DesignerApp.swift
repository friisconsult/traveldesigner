//
//  Travel_DesignerApp.swift
//  Travel Designer
//
//  Created by Per Friis on 02/11/2021.
//

import SwiftUI

@main
struct Travel_DesignerApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
